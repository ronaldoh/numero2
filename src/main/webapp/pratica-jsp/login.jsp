<%@page import="java.util.Date"%>
<jsp:useBean id="login" class="pratica.jsp.loginBean" scope="session"/>
<jsp:setProperty name="login" property="*"/>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Login</title>
    </head>
    <body>
        <form method="post" action="login.jsp">
            C�digo: <input type="text" name="login"/><br/>
            Nome: <input type="text" name="nome"/><br/>
            Perfil: <select name="perfil">
                <option value="1">Cliente</option>
                <option value="2">Gerente</option>
                <option value="3">Administrador</option>
            </select>
            <input type="submit" value="Enviar"/><br/>
            <%
                String strLogin = login.getLogin();
                String strNome = login.getNome();
                String profile = login.getPerfil();

                if ((strLogin != null) && (strNome != null)) {
                    if (strLogin.equals(strNome)) {
                        switch (profile) {
                            case "1":
                                login.setPerfil("Cliente");
                                break;
                            case "2":
                                login.setPerfil("Gerente");
                                break;
                            case "3":
                                login.setPerfil("Administrador");
                                break;
                            default:
                                out.println("Erro no perfil");
                        }
            %>
            <div>
                <font color="blue">${login.perfil}, login bem sucedido, para ${login.login} �s <%= new java.util.Date()%> </font>
            </div>
            <%  } else {  %>
            <div>
                <font color="red"><i>Acesso negado</i></font>               
            </div>

            <%  }
                }
            %>
        </form>
    </body>
</html>